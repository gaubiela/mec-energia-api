import pytest
from datetime import date
from universities.models import University, ConsumerUnit
from tariffs.models import Distributor
from contracts.models import Contract, EnergyBill

@pytest.fixture
def create_objects_for_testing():
    university_instance = University.objects.create(
    name="Minha Universidade",
)

    consumer_unit = ConsumerUnit.objects.create(
        name="Minha Consumer Unit",
        code="12345",  
        is_active=True,
        university=university_instance,  
    )

  
    distributor = Distributor.objects.create(
    name="Meu Distribuidor",
    is_active=True,
    university_id=university_instance.id,
    
)

    
    contract = Contract.objects.create(
        consumer_unit=consumer_unit,
        distributor=distributor,
        start_date=date(2023, 1, 1),
        end_date=None,
        tariff_flag='G',
        subgroup='AS',
        supply_voltage=2.0,
        peak_contracted_demand_in_kw=100.0,
        off_peak_contracted_demand_in_kw=50.0,
    )

    
    energy_bill = EnergyBill.objects.create(
        contract=contract,
        consumer_unit=consumer_unit,
        date=date(2023, 2, 1),
        invoice_in_reais=150.0,
        is_atypical=False,
        peak_consumption_in_kwh=200.0,
        off_peak_consumption_in_kwh=100.0,
        peak_measured_demand_in_kw=120.0,
        off_peak_measured_demand_in_kw=60.0,
    )

    return consumer_unit, distributor, contract, energy_bill

@pytest.mark.django_db
def test_create_energybill_with_existing_month_year(create_objects_for_testing):
    consumer_unit, distributor, contract, existing_energy_bill = create_objects_for_testing

    # Criando outra EnergyBill para a mesma UC no mesmo mês e ano
    with pytest.raises(Exception, match='Já existe uma fatura de energia para esta unidade de consumo neste mês e ano'):
        EnergyBill.objects.create(
            contract=contract,
            consumer_unit=consumer_unit,
            date=date(2023, 2, 1),
            invoice_in_reais=200.0,
            is_atypical=False,
            peak_consumption_in_kwh=250.0,
            off_peak_consumption_in_kwh=120.0,
            peak_measured_demand_in_kw=150.0,
            off_peak_measured_demand_in_kw=70.0,
        )

    # Verifica se a EnergyBill existente ainda está lá
    assert EnergyBill.objects.filter(
        consumer_unit=consumer_unit,
        date__month=2,
        date__year=2023
    ).count() == 1

@pytest.mark.django_db
def test_not_create_energybill_with_different_contract_same_month_year(create_objects_for_testing):
    consumer_unit, distributor, contract, existing_energy_bill = create_objects_for_testing

    # Criando um novo contrato para a mesma UC
    new_contract = Contract.objects.create(
        consumer_unit=consumer_unit,
        distributor=distributor,
        start_date=date(2023, 1, 1),
        end_date=None,
        tariff_flag='G',
        subgroup='AS',
        supply_voltage=2.0,
        peak_contracted_demand_in_kw=200.0,
        off_peak_contracted_demand_in_kw=100.0,
    )

    # Tentando criar outra EnergyBill para a mesma UC no mesmo mês e ano, mas com um contrato diferente
    with pytest.raises(Exception, match='Já existe uma fatura de energia para esta unidade de consumo neste mês e ano'):
        EnergyBill.objects.create(
            contract=new_contract,  # contrato diferente
            consumer_unit=consumer_unit,
            date=date(2023, 2, 1),
            invoice_in_reais=200.0,
            is_atypical=False,
            peak_consumption_in_kwh=250.0,
            off_peak_consumption_in_kwh=120.0,
            peak_measured_demand_in_kw=150.0,
            off_peak_measured_demand_in_kw=70.0,
        )

    # Verifica se nenhuma nova EnergyBill foi criada
    assert EnergyBill.objects.filter(
        contract=new_contract,
        consumer_unit=consumer_unit,
        date__month=2,
        date__year=2023
    ).count() == 0


