# tests/test_user_type_util.py

import re
import pytest
from utils.user.user_type_util import UserType
from users.models import CustomUser, UniversityUser

@pytest.fixture
def user_type_instance():
    return UserType()

#CT1
def test_invalid_user_type(user_type_instance):
    with pytest.raises(Exception, match=r'User type ".+" does not exist') as exc_info:
        user_type_instance.is_valid_user_type("invalid_type")

    assert 'User type "invalid_type" does not exist' in str(exc_info.value)

#CT2
def test_valid_university_user_type(user_type_instance):
    # Assuming university_user_type is a valid type in CustomUser.university_user_types
    user_type = user_type_instance.is_valid_user_type("university_user")
    assert user_type == "university_user"

#CT3
def test_valid_custom_user_type(user_type_instance):
    user_type = user_type_instance.is_valid_user_type("super_user")
    assert user_type == "super_user"

#CT4
def test_wrong_user_type_for_university_user(user_type_instance):
    # Tipo de usuário que esteja em all_user_types mas não em university_user_types
    wrong_type = "super_user"
    with pytest.raises(Exception, match=re.escape(f'Wrong User type "{wrong_type}" for this Model User ({str(UniversityUser)})')):
        user_type_instance.is_valid_user_type(wrong_type, user_model=UniversityUser)

#CT5
def test_wrong_user_type_for_custom_user(user_type_instance):
    # Tipo de usuário que esteja em all_user_types mas não seja igual a super_user_type
    wrong_type = "university_user"
    with pytest.raises(Exception, match=re.escape(f'Wrong User type "{wrong_type}" for this Model User ({str(CustomUser)})')):
        user_type_instance.is_valid_user_type(wrong_type, user_model=CustomUser)






